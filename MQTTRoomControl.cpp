#include "MQTTRoomControl.hpp"
#include <cassert>
#include <sstream>
#include "tinyosc.h"
#include <boost/bind/bind.hpp>

using namespace boost::placeholders;

const std::string MQTTRoomControl::signal_reset ("RESET");
const std::chrono::milliseconds MQTTRoomControl::debounce_duration (50);
const unsigned MQTTRoomControl::num_of_switches (8);
const std::string MQTTRoomControl::topic_magnets  ("magnets");
const std::string MQTTRoomControl::topic_switches ("switches");

MQTTRoomControl::MQTTRoomControl
(
    boost::asio::io_context &ioc,
    const std::string &broker_hostname,
    std::uint16_t broker_port,
    const std::string &node_name,
    // const std::string &serial_device,
    // const firmata::baud_rate &baud,
    const std::vector<firmata::pin*> &switches,
    const std::vector<firmata::pin*> &magnets,
    unsigned ola_universe,
    const std::string &qlab_hostname,
    uint16_t qlab_port
)
    : MQTTNode(ioc, broker_hostname, broker_port, node_name),
      // device(ioc, serial_device), arduino(this->device),
      debounce(ioc, debounce_duration),
      switches((assert(switches.size() == num_of_switches), switches)),
      magnets((assert(magnets.size() == num_of_switches), magnets)),
      ola_universe(ola_universe),
      ola_client(ola::client::StreamingClient::Options()),
      qlab_hostname(qlab_hostname),
      qlab_port(qlab_port),
      full_topic_switches(node_name + "/" + topic_switches),
      full_topic_magnets(node_name + "/" + topic_magnets),
      switch_states(num_of_switches, 1),
      magnet_values(num_of_switches, 1),
      riddle_solved(num_of_switches, false)
{
    // device.set(baud);
}

static std::string generate_osc_packet(const std::string &cmd) {
    char osc_msg[512];
    osc_msg[0] = 0300;
    int32_t tosc_len = int32_t(tosc_writeMessage(osc_msg + 1, sizeof(osc_msg) - 2, cmd.data(), "") + 1);
    assert(tosc_len >= 1);
    osc_msg[tosc_len++] = 0300;
    return std::string(osc_msg, osc_msg + tosc_len);
}

void MQTTRoomControl::send_qlab_command(const std::string &cmd) {
    std::string osc_msg = generate_osc_packet(cmd);
    try {
        boost::asio::ip::tcp::resolver::results_type endpoints =
                boost::asio::ip::tcp::resolver(ioc).resolve(qlab_hostname, std::to_string(qlab_port));
        boost::asio::ip::tcp::socket socket(ioc);
        boost::asio::connect(socket, endpoints);
        boost::asio::write(socket, boost::asio::buffer(osc_msg));
        socket.close();
    }
    catch (const std::exception &e) {
        std::cout << "Cannot send cue command." << std::endl;
    }

    /*
    std::shared_ptr<boost::asio::ip::tcp::resolver> resolver(new boost::asio::ip::tcp::resolver(ioc));
    resolver->async_resolve(qlab_hostname, std::to_string(qlab_port),
                            [resolver] (const boost::system::error_code &ec, boost::asio::ip::tcp::resolver::results_type result) {
        boost::asio::ip::tcp::socket socket(ioc);
        boost::asio::async_connect(socket, endpoints, [] (const boost::system::error_code &ec) {

        });
    });
    */
}

void MQTTRoomControl::switch_change_handler(std::uint8_t switch_index, std::uint8_t state) {
    assert(switch_index < num_of_switches);
    std::cout << "Switch " << int(switch_index) << " is " << (state ? "HIGH" : "LOW") << "." << std::endl;
    switch_states[switch_index] = state;
    publish_switch_states();
    if (!switch_states[switch_index] && !riddle_solved[switch_index]) {
        riddle_solved[switch_index] = true;
        magnets[switch_index]->value(0);
        magnet_values[switch_index] = 0;
        publish_magnet_values();
        ola::DmxBuffer dmx_buffer;
        switch (switch_index) {
        case 0: {
            dmx_buffer.Blackout();
            dmx_buffer.SetChannel(0, 255);
            ola_client.SendDmx(ola_universe, dmx_buffer);
            send_qlab_command("/cue/490/start");
            break;
        }
        case 1: {
            send_qlab_command("/cue/430/start");
            break;
        }
        case 2: {
            send_qlab_command("/cue/431/start");
            break;
        }
        case 3: {
            send_qlab_command("/cue/419/start");
            break;
        }
        case 4: {
            send_qlab_command("/cue/413/start");
            break;
        }
        case 5: {
            send_qlab_command("/cue/413/stop");
            break;
        }
        default: {
        }
        }
    }
}

void MQTTRoomControl::pin_change_handler(firmata::pin *sw, std::uint8_t state) {
    std::uint8_t switch_index = pin_to_switch_index(sw);
    switch_change_handler(switch_index, state);
}

std::uint8_t MQTTRoomControl::pin_to_switch_index(firmata::pin *sw) {
    for (unsigned i = 0; i < num_of_switches; i++) {
        if (switches[i] == sw) {
            return i;
        }
    }
    return -1;
}

static std::string pinvect2json(const std::vector<std::uint8_t> &vect) {
    std::ostringstream jsonss;
    jsonss << "{";
    for (unsigned i = 0; i < vect.size(); i++) {
        if (i) {
            jsonss << ", ";
        }
        jsonss << '"' << i << "\":" << int(vect[i]);
    }
    jsonss << "}";
    return jsonss.str();
}

void MQTTRoomControl::publish_switch_states() {
    std::cout << "Creating JSON string..." << std::endl;
    std::string switch_states_json = pinvect2json(switch_states);
    std::cout << "Done. Publishing asynchronously..." << std::endl;
    // mqtt_client->async_publish(full_topic_magnets, switch_states_json, mqtt::retain::yes);
    mqtt_client->async_publish(full_topic_switches, switch_states_json, mqtt::retain::yes);
    std::cout << "Done." << std::endl;
}

void MQTTRoomControl::publish_magnet_values() {
    std::string magnet_values_json = pinvect2json(magnet_values);
    mqtt_client->async_publish(full_topic_magnets, magnet_values_json, mqtt::retain::yes);
}

void MQTTRoomControl::reset() {
    riddle_solved = std::vector<bool>(num_of_switches, false);
    for (unsigned i = 0; i < num_of_switches; i++) {
        magnets[i]->value(1);
        magnet_values[i] = 1;
    }
    publish_magnet_values();
    ola::DmxBuffer blackout;
    blackout.Blackout();
    if (!ola_client.SendDmx(ola_universe, blackout)) {
        std::cout << "Cannot send DMX scene." << std::endl;
        exit(1);
    }
}

void MQTTRoomControl::start() {
    MQTTNode::start();
    if (!ola_client.Setup()) {
        std::cout << "Cannot connect to OLA server!" << std::endl;
        exit(1);
    }
    else {
        std::cout << "Connected to OLA server." << std::endl;
    }
    ola::DmxBuffer blackout;
    blackout.Blackout();
    if (!ola_client.SendDmx(ola_universe, blackout)) {
        std::cout << "Cannot send DMX scene." << std::endl;
        exit(1);
    }
    // I am assuming that the pin state always returns LOW in the beginning.
    std::cout << "Setting switches to digital input pullup..." << std::endl;
    for (firmata::pin *sw : switches) {
        sw->mode(firmata::mode::pullup_in);
    }
    std::cout << "Setting magnets to digital output..." << std::endl;
    for (firmata::pin *magnet : magnets) {
        magnet->mode(firmata::mode::digital_out);
        magnet->value(1);
    }
    std::cout << "All pins set up." << std::endl;
    /*
    ioc.poll();
    std::cout << "Checking all switches, maybe some are already low..." << std::endl;
    for (unsigned i = 0; i < num_of_switches; i++) {
        // In case a switch is already low.
        switch_change_handler(i, switches[i]->state());
    }
    std::cout << "Done." << std::endl;
    */
    std::cout << "Setting all switch handlers..." << std::endl;
    for (firmata::pin *sw : switches) {
        debounce.on_state_changed(*sw, boost::bind(&MQTTRoomControl::pin_change_handler, this, sw, _1));
    }
    std::cout << "Done." << std::endl;
}

void MQTTRoomControl::connection_established_handler() {
    MQTTNode::connection_established_handler();
    publish_switch_states();
    publish_magnet_values();
}

bool MQTTRoomControl::interpret_msg(const mqtt::buffer &topic, const mqtt::buffer &content) {
    if (!MQTTNode::interpret_msg(topic, content)) {
        if (topic == full_topic_signal && content == signal_reset) {
            reset();
            return true;
        }
    }
    return false;
}
