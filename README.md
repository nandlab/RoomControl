# RoomControl
## Dependencies
The packages `cmake` and `libboost-system-dev` are required for building. Install them with:

`sudo apt install cmake libboost-system-dev`

You can use [RoomAdmin](https://gitlab.com/NANDLAB/RoomAdmin) to monitor the MQTT devices.

## Cloning this repository
Use the `--recursive` option to also clone the submodules:

`git clone --recursive https://gitlab.com/NANDLAB/RoomControl.git`

## Build
Enter the RoomControl directory: `cd RoomControl`

Generate build configuration: `cmake .`

Actually build: `cmake --build .`

## Add desktop icon
I assume that the repository has been cloned to `/home/pi/RoomControl`. If this is not the case, adjust the `Exec` path in the RoomControl.desktop file.

Copy desktop file to the desktop: `cp -t ~/Desktop RoomControl.desktop`

## Add to autostart

Create the autostart directory: `mkdir -p ~/.config/autostart`

Copy desktop file to autostart: `cp -t ~/.config/autostart RoomControl.desktop`
